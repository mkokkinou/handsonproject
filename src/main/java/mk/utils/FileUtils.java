package mk.utils;

import java.io.*;
import java.util.*;

/**
 * Offers utility methods for manipulating files.
 */
public class FileUtils {
    /**
     * This method reads a file from the source file path
     * and returns the lines as a Set of unique ordered String objects
     *
     * @param srcFilePath the source file
     * @return a Set of unique ordered String objects
     * @throws java.io.IOException
     */
    public static Set<String> readUniqueLinesOrdered(String srcFilePath) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(srcFilePath), "utf-8"));
        Set<String> lines = new TreeSet<String>();
        String line;
        try {
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } finally {
            in.close();
        }
        return lines;
    }

    /**
     * This method receives a Collection of String objects
     * and prints this collection to a destination file (a word per line).
     *
     * @param lines       the common words
     * @param outFilePath the outFilePath
     * @throws IOException
     */

    public static void writeLinesToFile(Collection<String> lines, String outFilePath) throws IOException {
        OutputStreamWriter out = null;
        try {
            out = new OutputStreamWriter(new FileOutputStream(outFilePath), "UTF-8");
            for (String line : lines) {
                out.write(line);
                out.write("\n");
            }
        } finally {

            if (out != null) {
                out.close();
            }

        }
    }
}
