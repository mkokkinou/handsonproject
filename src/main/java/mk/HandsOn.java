package mk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

import static mk.utils.FileUtils.readUniqueLinesOrdered;
import static mk.utils.FileUtils.writeLinesToFile;

/**
 * The program will be started from the command line.
 * The input will be 3 arguments which are all paths to plain text files.
 * The first 2 paths point to input files having one word per line.
 * The program should write to the 3rd file only the words that are common in the two input files,
 * lexicographically ordered.
 */
public class HandsOn {

    private static Logger log = LoggerFactory.getLogger(HandsOn.class);

    public static void main(String[] args) {


        if (args.length != 3) {

            System.out.println("Usage: java -jar handsOnProject.jar <file1> <file2> <outputfIle>");

            System.out.println("<file1>: first input text file");

            System.out.println("<file2>: second input text file");

            System.out.println("<outputfIle>: file containing common words from both files");

        } else {

            try {

                Set<String> firstSet = readUniqueLinesOrdered(args[0]);
                Set<String> secondSet = readUniqueLinesOrdered(args[1]);

                firstSet.retainAll(secondSet);

                writeLinesToFile(firstSet, args[2]);

            } catch (IOException e) {

                log.error(" An error occurred while reading/writing to the file::" + e.getMessage());
            }
        }
    }
}


