1. What is the maximum number of words in the input files that your project can handle ? Assume that the program was started with -Xmx256M and that each word has on average 6 characters.

About 10M lines to be on the safe side.

2. What would you do to increase this limit ?

Increase Xmx

3. Is your program efficient ?

No, running time is about (O n ^ 2)

4. What would you do to increase its execution performance ?

Use a better algorithm, to avoid running the two nested loops.
